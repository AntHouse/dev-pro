<?php

$arr = [1, 2, [2, 5, 3, [32, 5], 3], 7, 4];
$sum = 0;

function foo($arr){

    if (gettype($arr) == "integer"){
        return $arr;
    }
    $sum = 0;
    foreach ($arr as $key => $value){
        $sum+= foo($value);
    }
    return $sum;
}

echo (foo($arr));