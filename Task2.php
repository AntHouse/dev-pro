<?php

interface TransportFactory
{
    public function createTransport();
}

interface Transport
{
    public function forward();
    public function backward();
    public function leftTurn();
    public function rightTurn();
}

class CarFactory implements TransportFactory
{
    public function createTransport()
    {
        return new CarMove();
    }
}

class BicycleFactory implements TransportFactory
{
    public function createTransport()
    {
        return new BicycleMove();
    }
}

class MotorcycleFactory implements TransportFactory
{
    public function createTransport()
    {
        return new MotorcycleMove();
    }
}

class TruckFactory implements TransportFactory
{
    public function createTransport()
    {
        return new TruckMove();
    }
}

class CarMove implements Transport
{
    public function backward()
    {
        return "Create Car backward";
    }

    public function forward()
    {
        // TODO: Implement forward() method.
    }

    public function leftTurn()
    {
        // TODO: Implement leftTurn() method.
    }

    public function rightTurn()
    {
        // TODO: Implement rightTurn() method.
    }
}

class BicycleMove implements Transport
{
    public function backward()
    {
        return "Create Bicycle backward";
    }

    public function forward()
    {
        // TODO: Implement forward() method.
    }

    public function leftTurn()
    {
        // TODO: Implement leftTurn() method.
    }

    public function rightTurn()
    {
        // TODO: Implement rightTurn() method.
    }
}

class MotorcycleMove implements Transport
{
    public function backward()
    {
        return "Create Motorcycle backward";
    }

    public function forward()
    {
        // TODO: Implement forward() method.
    }

    public function leftTurn()
    {
        // TODO: Implement leftTurn() method.
    }

    public function rightTurn()
    {
        // TODO: Implement rightTurn() method.
    }
}

class TruckMove implements Transport
{
    public function backward()
    {
        return "Create Truck backward";
    }

    public function forward()
    {
        // TODO: Implement forward() method.
    }

    public function leftTurn()
    {
        // TODO: Implement leftTurn() method.
    }

    public function rightTurn()
    {
        // TODO: Implement rightTurn() method.
    }
}

$car = new CarFactory();
echo $car->createTransport()->backward() . PHP_EOL;

$bicycle = new BicycleFactory();
echo $bicycle->createTransport()->backward() . PHP_EOL;

$motorcycle = new MotorcycleFactory();
echo $motorcycle->createTransport()->backward() . PHP_EOL;

$Truck = new TruckFactory();
echo $Truck->createTransport()->backward();
