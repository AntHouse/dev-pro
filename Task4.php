<?php

$value = '*';
$arr1 = [2, 5, 3, 7, 8, 1, 1, 1];
$arr2 = [3, 7, 6, 4, 6, 5, 1, 1];
$res = [];

for ($i = 0; $i < count($arr1); $i++){
    for($j = 0; $j < count($arr2); $j++){
        if ($arr1[$i] == $arr2[$j] && $arr1[$i]!=$value){
            $res[] = $arr1[$i];
            $arr1[$i] = $value;
            $arr2[$j] = $value;
        }
    }
}

echo "Result array: [" . implode(", ", $res) . "]";