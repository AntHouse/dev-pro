<?php

class Human
{
    const TYPE_DRIVER = 1;
    const TYPE_PASSENGER = 2;

    private $name;
    private $type;

    public function __construct($name, $type)
    {
        $this->name = $name;
        $this->type = $type;
    }

    public function getName()
    {
        return $this->name;
    }

    public static function createDriver($name)
    {
        return new self($name, self::TYPE_DRIVER);
    }

    public static function createPassenger($name)
    {
        return new self($name, self::TYPE_PASSENGER);
    }

    public function getType()
    {
        if($this->type == 1){
            return "Driver";
        } elseif ($this->type == 2){
            return "Passenger";
        } else {
            return "Incorrect type";
        }
    }
}
// First method
$human = new Human('Vasya', Human::TYPE_PASSENGER);
// Second method
$human2 = Human::createDriver('Petya');
echo $human->getType() . PHP_EOL;
echo $human2->getType();