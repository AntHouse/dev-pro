<?php

class Transport
{
    private $speed;
    //TODO:: Add driver and passenger
    //private $driver = [];
    //private $passenger = [];

    public function increasSpeed($speed)
    {
        return $this->speed +=$speed;
    }

    public function decreasSpeed($speed)
    {
        return $this->speed -=$speed;
    }

    public function getSpeed()
    {
        return $this->speed;
    }

    public function forward()
    {
        return "I am ". static::NAMECLASS . ". I am going forward";
    }

    public function backward()
    {
        return "I am ". static::NAMECLASS . ". I am going backward";
    }

    public function leftTurn()
    {
        return "I am ". static::NAMECLASS . ". I am turn left";
    }

    public function rightTurn()
    {
        return "I аm ". static::NAMECLASS . ". I am right";
    }
}

class Car extends Transport
{
    const NAMECLASS = __CLASS__;
}

class Bicycle extends Transport
{
    const NAMECLASS = __CLASS__;
}

class Motorcycle extends Transport
{
    const NAMECLASS = __CLASS__;
}

class Truck extends Transport
{
    const NAMECLASS = __CLASS__;
}

$car = new Car();
$car->increasSpeed(20);
$car->decreasSpeed(7);
echo $car->getSpeed() . PHP_EOL;

$truck = new Truck();
echo $truck->backward() . PHP_EOL;
echo $truck->leftTurn() . PHP_EOL;